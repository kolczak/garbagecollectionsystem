package sma.modules;

public abstract class BaseModule {
    private boolean initialized = false;
    public void initialize(Object...objects) {
        if (doInitialize(objects))
            initialized = true;
    };
    public boolean isInitialized() {
        return initialized;
    }
    abstract protected boolean doInitialize(Object...objects);
}
