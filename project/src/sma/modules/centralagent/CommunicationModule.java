package sma.modules.centralagent;

import jade.lang.acl.ACLMessage;

import java.io.IOException;

import sma.CentralAgent;
import sma.modules.BaseCommunicationModule;
import sma.ontology.Cell;
import sma.ontology.InfoAgent;
import sma.ontology.InfoGame;

/**
 * Communication Module of Central Agent. It both receives messages and sends
 * messages.
 * 
 * @author kolczak
 * 
 */
public class CommunicationModule extends BaseCommunicationModule {

    /**
     * 
     */
    private static final long serialVersionUID = 4719349873891700715L;

    public CommunicationModule(CentralAgent caCentralAgent) {
        super(caCentralAgent);
    }

    @Override
    protected void handleInform(ACLMessage msg) {
        try {
            if (msg.getOntology().equalsIgnoreCase("MOVE-SCOUT")) {
                this.myAgent.showMessage("Move Scout: " + msg.getContent());
                DiscoverGarbage.getInstance().discoverGabageNearCell(msg);
            } else if (msg.getOntology().equalsIgnoreCase("MOVE-HARVESTER")) {
                this.myAgent.showMessage("Move Harvester: " + msg.getContent());
                CollectGarbage.getInstance().CollectGarbageNearCell(msg);
            } else if (msg.getOntology().equalsIgnoreCase("GET-MAP")) {
                ACLMessage reply = new ACLMessage(ACLMessage.INFORM);
                try {
                    reply.setContentObject(InfoGame.getInstance().getInfo());
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                reply.setOntology(msg.getContent());
                reply.addReceiver(msg.getSender());
                myAgent.send(reply);
            }
        } catch (Exception e) {
            // System.out.println("ERROR ON CENTRAL"+e.getMessage());
        }

    }

    @Override
    protected void handleRequest(ACLMessage msg) {
        // TODO Auto-generated method stub

    }
}
