package sma.modules.coordinatoragent;

import jade.core.AID;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

import java.io.IOException;

import sma.CoordinatorAgent;
import sma.UtilsAgents;
import sma.modules.BaseCommunicationModule;
import sma.ontology.AuxInfo;
import sma.ontology.Cell;
import sma.ontology.InfoAgent;

/**
 * Communication Module of Coordinator Agent. It both receives messages and
 * sends messages.
 * 
 * @author kolczak
 * 
 */
public class CommunicationModule extends BaseCommunicationModule {

    /**
     * 
     */
    private static final long serialVersionUID = -8854115629148746231L;

    private AID cAgent = null;
    // public ArrayList<Object> localHarvesterList = new ArrayList<Object>();
    // public ArrayList<Object> localScoutList = new ArrayList<Object>();
    public AuxInfo localHarvesterList;
    public AuxInfo localScoutList;

    public CommunicationModule(CoordinatorAgent caCoordinatorAgent) {
        super(caCoordinatorAgent);
    }

    @Override
    protected void handleInform(ACLMessage msg) {
        try {
            // this.myAgent.showMessage("INFORM: " + msg.getContent());
            // System.out.println();
            if (msg.getOntology().equalsIgnoreCase("UPDATE-MAP")) {
                // System.out.println("Update: Map From chentral");
                localHarvesterList = (AuxInfo) msg.getContentObject();
                localScoutList = (AuxInfo) msg.getContentObject();
            } else if (msg.getOntology().equalsIgnoreCase("Harvester")) {
                ServiceDescription searchCriterion = new ServiceDescription();
                searchCriterion.setType(UtilsAgents.CENTRAL_AGENT);
                cAgent = UtilsAgents.searchAgent(this.myAgent, searchCriterion);
                this.send(ACLMessage.INFORM, cAgent, "English", "GET-MAP", "GET-MAP-HARVESTER", null);
            } else if (msg.getOntology().equalsIgnoreCase("Scout")) {
                ServiceDescription searchCriterion = new ServiceDescription();
                searchCriterion.setType(UtilsAgents.CENTRAL_AGENT);
                cAgent = UtilsAgents.searchAgent(this.myAgent, searchCriterion);
                this.send(ACLMessage.INFORM, cAgent, "English", "GET-MAP", "GET-MAP-SCOUT", null);
            } else if (msg.getOntology().equalsIgnoreCase("GET-MAP-SCOUT")) {
                ServiceDescription searchCriterion = new ServiceDescription();
                searchCriterion.setType(UtilsAgents.SCOUT_COORDINATOR_AGENT);
                cAgent = UtilsAgents.searchAgent(this.myAgent, searchCriterion);
                try {
                    localScoutList = (AuxInfo) msg.getContentObject();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // this.send(ACLMessage.INFORM, cAgent, "English",
                // "COORDINATOR-MAP-UPDATE", localScoutList, null);
                ACLMessage msg1 = new ACLMessage(ACLMessage.INFORM);
                msg1.addReceiver(cAgent);
                msg1.setLanguage("English");
                msg1.setOntology("COORDINATOR-MAP-UPDATE");
                if (localScoutList != null) {
                    try {
                        msg1.setContentObject(localScoutList);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                myAgent.send(msg1);
            } else if (msg.getOntology().equalsIgnoreCase("GET-MAP-HARVESTER")) {
                ServiceDescription searchCriterion = new ServiceDescription();
                searchCriterion.setType(UtilsAgents.HARVESTER_COORDINATOR_AGENT);
                cAgent = UtilsAgents.searchAgent(this.myAgent, searchCriterion);
                try {
                    localHarvesterList = (AuxInfo) msg.getContentObject();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.send(ACLMessage.INFORM, cAgent, "English", "COORDINATOR-MAP-UPDATE", "", localHarvesterList);
            } else if (msg.getOntology().equalsIgnoreCase("MOVE-SCOUT")) {
                // System.out.println("Scount to Coordinator: " +
                // msg.getContent());

                ServiceDescription searchCriterion = new ServiceDescription();
                searchCriterion.setType(UtilsAgents.CENTRAL_AGENT);
                cAgent = UtilsAgents.searchAgent(this.myAgent, searchCriterion);
                this.send(ACLMessage.INFORM, cAgent, "English", "MOVE-SCOUT", msg.getContent(), null);
            } else if (msg.getOntology().equalsIgnoreCase("MOVE-HARVESTER")) {
                System.out.println("Harvester to Coordinator: " + msg.getContent());

                ServiceDescription searchCriterion = new ServiceDescription();
                searchCriterion.setType(UtilsAgents.CENTRAL_AGENT);
                cAgent = UtilsAgents.searchAgent(this.myAgent, searchCriterion);
                this.send(ACLMessage.INFORM, cAgent, "English", "MOVE-HARVESTER", msg.getContent(), null);
            } else if (msg.getOntology().equalsIgnoreCase("HARVESTER-COLLECT-GARBAGE")) {
            	ServiceDescription searchCriterion = new ServiceDescription();
                searchCriterion.setType(UtilsAgents.HARVESTER_COORDINATOR_AGENT);
                cAgent = UtilsAgents.searchAgent(this.myAgent, searchCriterion);
                InfoAgent ia = null;
                try {
                	ia = (InfoAgent) msg.getContentObject();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.send(ACLMessage.INFORM, cAgent, "English", "HARVESTER-COLLECT-GARBAGE", "", ia);
            }
            else if (msg.getOntology().equalsIgnoreCase("HARVESTER-COLLECT-GARBAGE-FAILD")) {
            	ServiceDescription searchCriterion = new ServiceDescription();
                searchCriterion.setType(UtilsAgents.HARVESTER_COORDINATOR_AGENT);
                cAgent = UtilsAgents.searchAgent(this.myAgent, searchCriterion);
                InfoAgent ia = null;
                try {
                	ia = (InfoAgent) msg.getContentObject();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.send(ACLMessage.INFORM, cAgent, "English", "HARVESTER-COLLECT-GARBAGE-FAILD", "", ia);
            }
            else if (msg.getOntology().equalsIgnoreCase("FOUND-GARBAGE")) {
                System.out.println("FOUND GARBAGE 111111111111111111111111111111111111111111111111");
                ServiceDescription searchCriterion = new ServiceDescription();
                searchCriterion.setType(UtilsAgents.HARVESTER_COORDINATOR_AGENT);
                cAgent = UtilsAgents.searchAgent(this.myAgent, searchCriterion);
                Cell garbage = null;
                try {
                	garbage = (Cell) msg.getContentObject();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.send(ACLMessage.INFORM, cAgent, "English", "FOUND GARBAGE", "", garbage);
            }

        } catch (Exception e) {
            System.out.println("ERROR THERE");
        }
    }

    @Override
    protected void handleRequest(ACLMessage msg) {
    }
}
