package sma.modules.harvestercoordinatoragent;

import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import sma.BaseAgent;
import sma.modules.BaseCommunicationModule;
import sma.ontology.AuxInfo;
import sma.ontology.Cell;
import sma.ontology.InfoAgent;

public class CommunicationModule extends BaseCommunicationModule{
    
    public CommunicationModule(BaseAgent baseAgent) {
        super(baseAgent);
    }

    private static final long serialVersionUID = -5161905906003607740L;

    @Override
    protected void handleInform(ACLMessage msg) {
        try {           
        	if(msg.getOntology().equalsIgnoreCase("HARVESTER-COLLECT-GARBAGE"))
        	{
        		InfoAgent info  = (InfoAgent) msg.getContentObject();
        		MoveStrategyModule.getInstance().sendHarvesterToRecyclingCentre(info);
        		GarbageManagerModule.getInstance().garbageProperlyCollected(info);
        	}
        	else if(msg.getOntology().equalsIgnoreCase("FOUND GARBAGE"))
        	{
        		Cell garbagesList  =  (Cell) msg.getContentObject();
        		GarbageManagerModule.getInstance().newGarbagesFound(garbagesList);
        	}
        	else if(msg.getOntology().equalsIgnoreCase("HARVESTER-COLLECT-GARBAGE-FAILD"))
        	{
        	    System.out.println("ERROR: uncollected garbage, reassign");
        		InfoAgent info  = (InfoAgent) msg.getContentObject();
        		GarbageManagerModule.getInstance().garbageUnproperlyCollected(info);
        	}
        	else
        	{
	            AuxInfo info = (AuxInfo) msg.getContentObject();
	            // showMessage("Agents initial position: ");
	            if (info instanceof AuxInfo) {
	                MoveStrategyModule.getInstance().moveHarvesters(info, msg.getSender());
	            }
        	}
            //TODO: if we receive new garbages (from scoutCoordinator)
            //GarbageManagerModule.getInstance().newGarbagesFound(garbagesList);

        } catch (UnreadableException e) {
            // TODO Auto-generated catch block
            System.out.println("Harvester error");
            e.printStackTrace();
        }        
    }

    @Override
    protected void handleRequest(ACLMessage msg) {
        // TODO Auto-generated method stub
        
    }

}
