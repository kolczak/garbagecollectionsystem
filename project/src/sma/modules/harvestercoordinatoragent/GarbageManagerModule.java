package sma.modules.harvestercoordinatoragent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import sma.modules.BaseModule;
import sma.ontology.Cell;
import sma.ontology.InfoAgent;

//TODO: we need to clean assignedGarbages sometimes. When?
public class GarbageManagerModule extends BaseModule {

    private ArrayList<Cell> unassignedGarbages = new ArrayList<Cell>();
    HashMap<Cell, InfoAgent> assignedGarbages = new HashMap<Cell, InfoAgent>();
    
    @Override
    protected boolean doInitialize(Object... objects) {
        return false;
    }
    
//    /**
//     * Test method to check dijkstra work TODO: remove it
//     * @param aiInfo
//     */
//    private void generateGarbages(AuxInfo aiInfo) {
//        
//        if (assignedGarbages.size() == 0 && uncollectedGarbages.size() == 0) {
//            System.out.println("E");
//            //aiInfo.getBuildingsGarbage();
//            
//            Cell garbage = new Cell(Cell.BUILDING);
//            garbage.setColumn(2);
//            garbage.setRow(1);
//
//            char c = 'G';
//            try {
//                garbage.setGarbageType(c);
//                garbage.setGarbageUnits(2);
//            } catch (Exception e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//            aiInfo.getBuildingsGarbage().add(garbage);
//            
//            for(Cell ga: aiInfo.getBuildingsGarbage())
//            {
//                try{
//                    System.out.println("Garbage:"+ga.getRow()+" "+ga.getColumn()+" "+ga.getGarbageUnits()+" "+ga.getGarbageType());
//                }catch(Exception e){
//                    System.out.println(e.getMessage());
//                }
//            }
//        }
//    }

    /**
     * To assign found garbages to harvesters
     */
    public void assignFoundedGarbages() {
        synchronized (unassignedGarbages) {
            /* Added by Manas for checking the garbage collection */
            System.out.println("HARVESTER: Unassigned garbages size: " + unassignedGarbages.size());
            if (unassignedGarbages.size() == 0 && assignedGarbages.size() == 0) {
                Cell garbage1 = new Cell(Cell.BUILDING);
                garbage1.setRow(0);
                garbage1.setColumn(7);
                try {
                    garbage1.setGarbageUnits(10);
                    garbage1.setGarbageType('P');
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                unassignedGarbages.add(garbage1);
            }
            if (unassignedGarbages.size() != 0) {
                // System.out.println("ASSIGN: 1############3");
                MoveStrategyModule.getInstance().assignHarvesterToGarbage(unassignedGarbages);
            }
        }
    }
    
    /**
     * If there are new garbages found.
     * @param garbagesList
     */
    public void newGarbagesFound(Cell garbage) {
        System.out.println("New garbage: col" + garbage.getColumn() + "  row " + garbage.getRow());
        synchronized (unassignedGarbages) {
            if (!unassignedGarbages.contains(garbage) && !assignedGarbages.containsKey(garbage)) {
                unassignedGarbages.add(garbage);
            }

            if (unassignedGarbages.size() != 0) {
                // System.out.println("ASSIGN: 2############");
                MoveStrategyModule.getInstance().assignHarvesterToGarbage(unassignedGarbages);
            }
        }
    } 
    
    /**
     * If garbage was properly assigned prevent to assign another time.
     * @param garbage
     */
    public void garbageProperlyAssigned(Cell garbage, InfoAgent infoAgent) {
        //System.out.println("ASSIGN: 3############ removing from uncollected: " + garbage.toString() + "\n");
        synchronized (unassignedGarbages) {
            unassignedGarbages.remove(garbage);    
        }
        assignedGarbages.put(garbage, infoAgent);
    }
    
    /**
     * If garbage was properly collected by harvester remove from memory.
     * @param garbage
     */
    public void garbageProperlyCollected(InfoAgent agent) {
        Cell choosenGarbage = null;
        for (Cell garbage : assignedGarbages.keySet()) {
            if (assignedGarbages.get(garbage).equals(agent)) {
                choosenGarbage = garbage;
                break;
            }
        }
        assignedGarbages.remove(choosenGarbage);
    }
    
    /**
     * If garbage was properly collected by harvester remove from memory.
     * @param garbage
     */
    public void garbageUnproperlyCollected(InfoAgent agent) {
        Cell choosenGarbage = null;
        for (Cell garbage : assignedGarbages.keySet()) {
            if (assignedGarbages.get(garbage).equals(agent)) {
                choosenGarbage = garbage;
                break;
            }
        }
        assignedGarbages.remove(choosenGarbage);
        unassignedGarbages.add(choosenGarbage);
    }
    /**
     * Signleton part
     */

    private static GarbageManagerModule instance = new GarbageManagerModule();

    private GarbageManagerModule() {

    }

    public static GarbageManagerModule getInstance() {
        return instance;
    }
}
