package sma.modules;

import java.io.IOException;
import java.io.Serializable;

import sma.BaseAgent;
import sma.ontology.AuxInfo;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

public abstract class BaseCommunicationModule extends CyclicBehaviour {

    /**
     * 
     */
    private static final long serialVersionUID = 262823067137106181L;
    protected BaseAgent myAgent = null;

    public BaseCommunicationModule(BaseAgent baseAgent) {
        this.myAgent = baseAgent;
        this.myAgent.showMessage("CommunicationModule started");
    }

    @Override
    public void action() {
        ACLMessage msg = myAgent.receive();

        if (msg != null) {
            // Message received. Process it
            switch (msg.getPerformative()) {
            case ACLMessage.INFORM:
                try {
					handleInform(msg);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                break;
            case ACLMessage.REQUEST:
                handleRequest(msg);
                break;
            }
        }
    }

    /**
     * Common part responsible for sending messages.
     * @param myAgent
     * @param iPerformative
     * @param aidReceiver
     * @param sLanguage
     * @param sOntology
     * @param sContent
     * @param sObject Serializable object to send with message
     */
    public void send(int iPerformative, AID aidReceiver, String sLanguage, String sOntology, String sContent, Serializable serContentObject) {
        ACLMessage msg = new ACLMessage(iPerformative);
        msg.addReceiver(aidReceiver);
        msg.setLanguage(sLanguage);
        msg.setOntology(sOntology);
        msg.setContent(sContent);
        if (serContentObject != null) {
            try {
                msg.setContentObject(serContentObject);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.myAgent.send(msg);
    }
    
//    /**
//     * Common part responsible for sending messages.
//     * @param myAgent
//     * @param iPerformative
//     * @param aidReceiver
//     * @param sLanguage
//     * @param sOntology
//     * @param sContent
//     * @param sObject Serializable object to send with message
//     */
//    public void send(int iPerformative, AID aidReceiver, String sLanguage, String sOntology, AuxInfo sContent, Serializable serObject) {
//        ACLMessage msg = new ACLMessage(iPerformative);
//        msg.addReceiver(aidReceiver);
//        msg.setLanguage(sLanguage);
//        msg.setOntology(sOntology);
//        if (serObject != null) {
//            try {
//                msg.setContentObject(serObject);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        this.myAgent.send(msg);
//    }
    
    abstract protected void handleInform(ACLMessage msg) throws InterruptedException;
    abstract protected void handleRequest(ACLMessage msg);
}
