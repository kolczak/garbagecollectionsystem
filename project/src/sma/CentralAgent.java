package sma;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import sma.gui.GraphicInterface;
import sma.modules.centralagent.CommunicationModule;
import sma.ontology.Cell;
import sma.ontology.InfoAgent;
import sma.ontology.InfoGame;

/**
 * <p>
 * <B>Title:</b> IA2-SMA
 * </p>
 * <p>
 * <b>Description:</b> Practical exercise 2011-12. Recycle swarm.
 * </p>
 * <p>
 * <b>Copyright:</b> Copyright (c) 2011
 * </p>
 * <p>
 * <b>Company:</b> Universitat Rovira i Virgili (<a
 * href="http://www.urv.cat">URV</a>)
 * </p>
 * 
 * @author not attributable
 * @version 2.0
 */
public class CentralAgent extends BaseAgent {

    /**
     * 
     */
    private static final long serialVersionUID = -806624947060481909L;

    private static sma.gui.GraphicInterface gui;
    private sma.ontology.InfoGame game;
    private boolean is_communication_started = false;
    private AID coordinatorAgent;
    public static int total_point = 0;

    public static CommunicationModule _cmCommunicationModule = null;

    public CentralAgent() {
        super();
    }

    /**
     * A message is shown in the log area of the GUI
     * 
     * @param str
     *            String to show
     */
    @Override
    public void showMessage(String str) {
        if (gui != null)
            gui.showLog(str + "\n");
        super.showMessage(str);
    }

    public static void showStistic(String str) {
        if (gui != null)
            gui.showStatistics(str + "\n");
    }

    private java.util.List<Cell> placeAgents(InfoGame currentGame, Agent a) throws Exception {
        // Temporal version. Agents must be randomly placed
        java.util.List<Cell> agents = new java.util.ArrayList<Cell>();

        int[] row;
        InfoAgent b;
        // ut.createAgent(agentName, className, arguments);
        for (int k = 0; k < currentGame.getInfo().getNumScouts(); k++) {
            // prevent generating agents in the same position
            do {
                row = currentGame.getRandomXY();
            } while (((currentGame.getInfo().getMap())[row[0]][row[1]]).getAgent() != null);

            System.out.println("Row :" + row[0] + " Col:" + row[1]);
            b = new InfoAgent(InfoAgent.SCOUT);
            ((currentGame.getInfo().getMap())[row[0]][row[1]]).addAgent(b);
            agents.add(currentGame.getInfo().getCell(row[0], row[1]));
            // -------------------------------------------------------
            UtilsAgents.createAgent("ScoutAgent" + (k + 1), "sma.ScoutAgent", null);
            // -------------------------------------------------------
        }

        for (int k = 0; k < currentGame.getInfo().getNumHarvesters(); k++) {
            // prevent generating agents in the same position
            do {
                row = currentGame.getRandomXY();
            } while (((currentGame.getInfo().getMap())[row[0]][row[1]]).getAgent() != null);
            
            System.out.println("Row :" + row[0] + " Col:" + row[1]);
            InfoAgent p = new InfoAgent(InfoAgent.HARVESTER);
            p.setMaxUnits(currentGame.getInfo().getCapacityHarvesters()[k]);
            p.setGarbageTypes(currentGame.getInfo().getTypeHarvesters()[k]);
            ((currentGame.getInfo().getMap())[row[0]][row[1]]).addAgent(p);
            agents.add(currentGame.getInfo().getCell(row[0], row[1]));
            // -------------------------------------------------------
            UtilsAgents.createAgent("HarvesterAgent" + (k + 1), "sma.HarvesterAgent", null);
            // -------------------------------------------------------
        }
        return agents;
    }

    /**
     * Agent setup method - called when it first come on-line. Configuration of
     * language to use, ontology and initialization of behaviours.
     */
    protected void setup() {

        /**** Very Important Line (VIL) *********/
        this.setEnabledO2ACommunication(true, 1);
        /****************************************/

        // showMessage("Agent (" + getLocalName() + ") .... [OK]");

        // Register the agent to the DF
        ServiceDescription sd1 = new ServiceDescription();
        sd1.setType(UtilsAgents.CENTRAL_AGENT);
        sd1.setName(getLocalName());
        sd1.setOwnership(UtilsAgents.OWNER);
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.addServices(sd1);
        dfd.setName(getAID());
        try {
            DFService.register(this, dfd);
            showMessage("Registered to the DF");
        } catch (FIPAException e) {
            System.err.println(getLocalName() + " registration with DF unsucceeded. Reason: " + e.getMessage());
            doDelete();
        }

        /**************************************************/

        try {
            this.game = InfoGame.getInstance(); // object with the game data
            this.game.readGameFile("game.txt");
            // game.writeGameResult("result.txt", game.getMap());
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Game NOT loaded ... [KO]");
        }
        try {
            this.gui = new GraphicInterface(game);
            gui.setVisible(true);
            showMessage("Game loaded ... [OK]");
        } catch (Exception e) {
            e.printStackTrace();
        }

        showMessage("Buildings with garbage");
        for (Cell c : game.getBuildingsGarbage())
            showMessage(c.toString());

        /**** Agents are randomly placed ****************/

        java.util.List<Cell> agents = null;
        try {
            agents = placeAgents(this.game, this);
        } catch (Exception e) {
        }

        if (agents == null)
            System.out.println("NULL Error!");

        /**************************************************/
        this.game.getInfo().fillAgentsInitialPositions(agents);

        // If any scout is near a building with garbage, we show it in the
        // public map
        java.util.List<Cell> remove = new LinkedList<Cell>();
        for (Cell a : agents) {
            if (a.getAgent().getAgentType() == InfoAgent.SCOUT) {
                for (Cell b : game.getBuildingsGarbage()) {
                    int x = a.getRow();
                    int y = a.getColumn();
                    int xb = b.getRow();
                    int yb = b.getColumn();
                    if (x > 0) {
                        if ((xb == x - 1) && (yb == y)) {
                            game.getInfo().setCell(xb, yb, b);
                            remove.add(b);
                        }
                        if ((y > 0) && (xb == x - 1) && (yb == y - 1)) {
                            game.getInfo().setCell(xb, yb, b);
                            remove.add(b);
                        }
                        if ((y < game.getInfo().getMap()[x].length - 1) && (xb == x - 1) && (yb == y + 1)) {
                            game.getInfo().setCell(xb, yb, b);
                            remove.add(b);
                        }
                    }
                    if (x < game.getInfo().getMap().length - 1) {
                        if ((xb == x + 1) && (yb == y)) {
                            game.getInfo().setCell(xb, yb, b);
                            remove.add(b);
                        }
                        if ((y > 0) && (xb == x + 1) && (yb == y - 1)) {
                            game.getInfo().setCell(xb, yb, b);
                            remove.add(b);
                        }
                        if ((y < game.getInfo().getMap()[x].length - 1) && (xb == x + 1) && (yb == y + 1)) {
                            game.getInfo().setCell(xb, yb, b);
                            remove.add(b);
                        }
                    }
                    if ((y > 0) && (xb == x) && (yb == y - 1)) {
                        game.getInfo().setCell(xb, yb, b);
                        remove.add(b);
                    }
                    if ((y < game.getInfo().getMap()[x].length - 1) && (xb == x) && (yb == y + 1)) {
                        game.getInfo().setCell(xb, yb, b);
                        remove.add(b);
                    }
                }
            }
        }

        // this.game.getMap();
        /*
         * showMessage("Agents initial position: "); for (Cell a : agents) { int
         * x = a.getRow(); int y = a.getColumn(); // a.getAgent().g
         * if(a.getAgent().getAgentType() == 1) { boolean[] gerbageType =
         * a.getAgent().getGarbageType();
         * System.out.println(x+" "+y+" "+a.getAgent
         * ().getMaxUnits()+" G:"+gerbageType
         * [0]+", P:"+gerbageType[1]+", M:"+gerbageType
         * [2]+", A:"+gerbageType[3]); disktra dk = new disktra();
         * dk.generateMap( this.game, x, y );
         * 
         * for(Cell garbage: this.game.getBuildingsGarbage()) { try{
         * System.out.println
         * ("Garbage:"+garbage.getRow()+" "+garbage.getColumn()
         * +" "+garbage.getGarbageUnits()+" "+garbage.getGarbageType());
         * dk.generatePath( garbage.getRow() , garbage.getColumn() );
         * System.out.println(dk.path_stack.toString()); }catch(Exception e){
         * System.out.println(e.getMessage()); } }
         * 
         * } }
         */

        System.out.println("Generate Garbage:" + game.getBuildingsGarbage().toString());

        // for (Cell r : remove)
        // game.getBuildingsGarbage().remove(r);

        // search CoordinatorAgent
        ServiceDescription searchCriterion = new ServiceDescription();
        searchCriterion.setType(UtilsAgents.COORDINATOR_AGENT);
        this.coordinatorAgent = UtilsAgents.searchAgent(this, searchCriterion);

        startCommunicationModule();
        // searchAgent is a blocking method, so we will obtain always a correct
        // AID

        // add behaviours

        // we wait for the initialization of the game
        /*
         * MessageTemplate mt = MessageTemplate .and(MessageTemplate
         * .MatchProtocol(InteractionProtocol.FIPA_REQUEST),
         * MessageTemplate.MatchPerformative(ACLMessage.REQUEST)); final
         * RequestResponseBehaviour rb = new RequestResponseBehaviour(this,
         * null, this); showMessage("***************************************" +
         * rb.getExecutionState()); this.addBehaviour(rb);
         */
    } // endof setup

    /*************************************************************************/

    @Override
    public void startCommunicationModule() {
        // add communication module
        if (!is_communication_started) {
            _cmCommunicationModule = new CommunicationModule(this);
            this.addBehaviour(_cmCommunicationModule);
            is_communication_started = true;
        }

        // Setup finished. When the last inform is received, the agent itself
        // will add
        // a behavious to send/receive actions
        // TODO: remove below; just to test
        // wait some time and send message to central agent
        /*
         * final Timer tTimer = new Timer(); TimerTask ttTask = new TimerTask()
         * {
         * 
         * @Override public void run() { tTimer.cancel();
         * _cmCommunicationModule.send(ACLMessage.INFORM, coordinatorAgent,
         * "English", "Weather-forecast-ontology", "It's raining", null); } };
         * tTimer.schedule(ttTask, 10000);
         */
        final Timer tTimer = new Timer();
        TimerTask ttTask = new TimerTask() {
            @Override
            public void run() {
                tTimer.cancel();
                Thread th = new Thread();

                try {
                    while (true) {
                        try {
                            game.randomGenerate();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        th.sleep(3000);
                    }
                } catch (InterruptedException e) {
                    System.out.println("Thread interrupted!");
                    e.printStackTrace();
                }
            }
        };
        tTimer.schedule(ttTask, 3000);
    }
} // end of class AgentCentral
