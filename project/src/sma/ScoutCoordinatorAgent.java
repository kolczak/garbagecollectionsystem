package sma;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPANames.InteractionProtocol;
import jade.lang.acl.ACLMessage;
import jade.proto.AchieveREInitiator;

import java.util.Timer;
import java.util.TimerTask;

import sma.modules.scoutcoordinatoragent.CommunicationModule;
import sma.ontology.AuxInfo;
import sma.ontology.Cell;
import sma.ontology.InfoAgent;

public class ScoutCoordinatorAgent extends BaseAgent {
    private AuxInfo info;
    //private AID centralAgent;
    private AID coordinatorAgent;
    private CommunicationModule communicationModule = null;
    
    public ScoutCoordinatorAgent() {    
    }

	protected void setup() {
	        this.setEnabledO2ACommunication(true, 1);	        
	        // Register the agent to the DF
	        ServiceDescription sd1 = new ServiceDescription();
	        sd1.setType(UtilsAgents.SCOUT_COORDINATOR_AGENT);
	        sd1.setName(getLocalName());
	        sd1.setOwnership(UtilsAgents.OWNER);
	        DFAgentDescription dfd = new DFAgentDescription();
	        dfd.addServices(sd1);
	        dfd.setName(getAID());
	        try {
	            DFService.register(this, dfd);
	            showMessage("Registered Scout Coordinator to the DF");
	        } catch (FIPAException e) {
	            System.err.println(getLocalName() + " registration with DF "
	                    + "unsucceeded. Reason: " + e.getMessage());
	            doDelete();
	        }
	        try {
				Thread.sleep(6000);
			} catch (Exception e) {}
	        // search CentralAgent
	        ServiceDescription searchCriterion = new ServiceDescription();
	        searchCriterion.setType(UtilsAgents.COORDINATOR_AGENT);
	        this.coordinatorAgent = UtilsAgents.searchAgent(this, searchCriterion);
	        // searchAgent is a blocking method, so we will obtain always a correct
	        // AID
	        startCommunicationModule();
	        
	} 




    @Override
    public  void startCommunicationModule() {
     // add communication module
        this.communicationModule = new CommunicationModule(this);
        this.addBehaviour(this.communicationModule);

        // wait some time and send message to central agent
        final Timer tTimer = new Timer();
          
        TimerTask ttTask = new TimerTask()
        {
            @Override
            public void run()
            {
                tTimer.cancel();
                Thread th = new Thread();
                try{
                	while(true)
        	        {
        	        	communicationModule.send(ACLMessage.INFORM, coordinatorAgent, "English", "Scout","Scout", null);
        	        	th.sleep(1000);
        	        }
            	 }
            	  catch(InterruptedException e){
            	    System.out.println("Thread interrupted!");
            	    e.printStackTrace();
            	  }
            }
        };
        tTimer.schedule(ttTask,2000);
    	
    }
		 	         	
}
