package sma;

import jade.core.AID;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

import java.util.Timer;
import java.util.TimerTask;

import sma.modules.harvestercoordinatoragent.CommunicationModule;
import sma.modules.harvestercoordinatoragent.MoveStrategyModule;
import sma.ontology.AuxInfo;

public class HarvesterCoordinatorAgent extends BaseAgent{

    private AuxInfo info;
    private AID coordinatorAgent;
    private CommunicationModule communicationModule = null;
    public int a;
    
    public HarvesterCoordinatorAgent() {    
    }

	protected void setup() {
	        this.setEnabledO2ACommunication(true, 1);	        
	        // Register the agent to the DF
	        ServiceDescription sd1 = new ServiceDescription();
	        sd1.setType(UtilsAgents.HARVESTER_COORDINATOR_AGENT);
	        sd1.setName(getLocalName());
	        sd1.setOwnership(UtilsAgents.OWNER);
	        DFAgentDescription dfd = new DFAgentDescription();
	        dfd.addServices(sd1);
	        dfd.setName(getAID());
	        try {
	            DFService.register(this, dfd);
	            showMessage("Registered Harvester Coordinator to the DF");
	        } catch (FIPAException e) {
	            System.err.println(getLocalName() + " registration with DF "
	                    + "unsucceeded. Reason: " + e.getMessage());
	            doDelete();
	        }

	        try {
				Thread.sleep(6000);
			} catch (Exception e) {
			}
	        // search CentralAgent
	       
			ServiceDescription searchCriterion = new ServiceDescription();
	        searchCriterion.setType(UtilsAgents.COORDINATOR_AGENT);
	        this.coordinatorAgent = UtilsAgents.searchAgent(this, searchCriterion);
	        // searchAgent is a blocking method, so we will obtain always a correct
	        // AID
	       
	        /*ACLMessage requestInicial = new ACLMessage(ACLMessage.REQUEST);
	        requestInicial.clearAllReceiver();
	        requestInicial.addReceiver(this.coordinatorAgent);
	        requestInicial.setProtocol(InteractionProtocol.FIPA_REQUEST);
	        showMessage("Message OK");
	        try {
	            requestInicial.setContent("Initial request");
	            showMessage("Content OK " + requestInicial.getContent());
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	        */
	        // we add a behaviour that sends the message and waits for an answer
	        //this.addBehaviour(new RequesterBehaviour(this, requestInicial));
	        
	        
	        startCommunicationModule();
	        // setup finished. When we receive the last inform, the agent itself
	        // will add
	        // a behaviour to send/receive actions

	       

	    } // endof setup



    @Override
    public void startCommunicationModule() {
        // add communication module
        this.communicationModule = new CommunicationModule(this);
        MoveStrategyModule.getInstance().initialize(this.communicationModule);
        this.addBehaviour(this.communicationModule);

            // TODO: remove below; just to test
            // wait some time and send message to central agent
            final Timer tTimer = new Timer();
            TimerTask ttTask = new TimerTask()
            {
                @Override
                public void run()
                {
                    tTimer.cancel();
                    Thread th = new Thread();
                    try{
                    	while(true)
            	        {
                    		communicationModule.send(ACLMessage.INFORM, coordinatorAgent, "English", "Harvester", "Harvester", null);
            	        	th.sleep(500);
            	        }
                	 }
                	  catch(InterruptedException e){
                	    System.out.println("Thread interrupted!");
                	    e.printStackTrace();
                	  }
                
                }
            };
            tTimer.schedule(ttTask,2000);        
    } 	         	
}
