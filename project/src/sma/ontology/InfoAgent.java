package sma.ontology;

import jade.core.AID;

/**
 * <p><B>Title:</b> IA2-SMA</p>
 * <p><b>Description:</b> Practical exercise 2011-12. Recycle swarm.</p>
 * <p><b>Copyright:</b> Copyright (c) 2011</p>
 * <p><b>Company:</b> Universitat Rovira i Virgili (<a
 * href="http://www.urv.cat">URV</a>)</p>
 * @author David Isern & Joan Albert L�pez
 */
public class InfoAgent extends Object implements java.io.Serializable {

    private static int GLOBALCOUNTER = 0;
    public int id;
    
	static public int SCOUT = 0;
	static public int HARVESTER = 1;

	private int typeAgent = -1;
	private AID aid;

	private boolean[] garbageType = { false, false, false, false }; //(Glass, Plastic, Metal, Paper)

	private int maxUnits = 10;
	private int units = 0;

	static public int GLASS = 0;
	static public int PLASTIC = 1;
	static public int METAL = 2;
	static public int PAPER = 3;

	private int currentType = -1;

	private HarvesterStatus harvesterStatus = HarvesterStatus.RANDOM_MOVE;
	
	public enum HarvesterStatus {
        /*1*/RANDOM_MOVE,
        /*2*/READY_TO_COLLECT,
        /*3*/READY_TO_EMPTY,
        /*4*/ON_WAY_TO_GARBAGE,
        /*5*/ON_WAY_TO_RC,
    }
	
	public boolean[] getGarbageType() {
		return garbageType;
	}

	public void setGarbageType(boolean[] garbageType) {
		this.garbageType = garbageType;
	}

	public int getMaxUnits() {
		return maxUnits;
	}

	public void setMaxUnits(int maxUnits) {
		this.maxUnits = maxUnits;
	}

	public int getUnits() {
		return units;
	}

	public void setUnits(int units) {
		this.units = units;
	}

	public int getCurrentType() {
		return currentType;
	}

	public void setCurrentType(int type) throws Exception {
		//if ((type != GLASS) && (type != PLASTIC) && (type != METAL)&& (type != PAPER))
		//	throw new Exception("Unkown type");
		this.currentType = type;
	}
	

	
//	public boolean equals(InfoAgent a) {
//		return a.getAID().equals(this.aid);
//	}
	
	@Override
	public boolean equals(Object a) {
	    return (this.id == ((InfoAgent)a).id);
	}
	
	@Override
	public int hashCode() {
	    return this.id;
	}

	public AID getAID() {
		return this.aid;
	}

	public void setAID(AID aid) {
		this.aid = aid;
	}

	public int getAgentType() {
		return this.typeAgent;
	}

	public void setAgentType(int type) throws Exception {
		if ((type != SCOUT) && (type != HARVESTER))
			throw new Exception("Unkown type");
		this.typeAgent = type;
	}

	public String getAgent() throws Exception {
		if (typeAgent == SCOUT)
			return "S";
		if (typeAgent == HARVESTER)
			return "H";
		throw new Exception("Unkown type");
	}

	public String toString() {
		String str = "";
		str = "(info-agent (agent-type " + this.getAgentType() + ")";
		//str=str+"(aid "+ this.getAID().getLocalName()+")";
		if (this.getAgentType() == InfoAgent.HARVESTER) {
			if (this.getCurrentType() > -1)
				str += "type" + "(" + this.getCurrentType() + ")";
			str += "units" + "(" + this.getUnits() + "/" + this.getMaxUnits()
					+ ")";
		} else
			str += ")";
		str += "id:" + id + " " + super.toString() + " " + hashCode();
		
		return str;
	}

	/**
	 * Default constructor
	 * @param agentType int Type of the agent we want to save
	 * @param aid AID Agent identifier
	 * @throws Exception Errors in the assignation
	 */
	public InfoAgent(int agentType, AID aid) throws Exception {
		this.setAgentType(agentType);
		this.setAID(aid);
		id = GLOBALCOUNTER;
		GLOBALCOUNTER++;
	}

	/**
	 * Constructor for the information of the agent, without its AID
	 * @param agentType int
	 * @throws Exception Errors in the assignation
	 */
	public InfoAgent(int agentType) throws Exception {
		this.setAgentType(agentType);
		id = GLOBALCOUNTER;
        GLOBALCOUNTER++;
	}

    public void setGarbageTypes(String sType) {
        if (sType.contains("G")) //GLASS
            garbageType[0] = true;
        if (sType.contains("P")) //PLASTIC
            garbageType[1] = true;
        if (sType.contains("M")) //METAL
            garbageType[2] = true;
        if (sType.contains("A")) //PAPER
            garbageType[3] = true;
    }

    public void setHarvesterStatus(HarvesterStatus harvesterStatus) {
        this.harvesterStatus = harvesterStatus;
    }

    public HarvesterStatus getHarvesterStatus() {
        return harvesterStatus;
    }

} //endof class InfoAgent
