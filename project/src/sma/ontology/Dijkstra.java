package sma.ontology;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;

public class Dijkstra {
	 /**
     * @param args the command line arguments
     */
    int boardSize;
    int path[] = new int[400];
    int dist[] = new int[1600];
    int previous[]= new int[1600];
    public ArrayList<Integer> path_stack = new ArrayList<Integer>();
    
	
    public void generateMap(AuxInfo game, int initial_x, int initial_y) {
    	int x,y;
        int length = 400;
        int source = initial_x*20+initial_y;
        int nodes[]= new int[1600];
        int neighbour[] = new int[8];
        HashSet  q = new HashSet();
        boardSize = game.getMap().length;
        System.out.println("Source:"+source);
        for(int i=0;i<length;i++)
        {
        	dist[i]     = 90000;
            previous[i] = -1;
        }
        dist[source] = 0;
        
       // All nodes in the graph are unoptimized - thus are in Q
        for(int i=0;i<length;i++)
        {
            q.add(i);
        }
        
        
        while(!q.isEmpty())
        {
        	// Calculating the minimum
            int min=30000000,
            current_node = source;
            for(int i=0;i<length;i++)
            {
                if( dist[i]<min && q.contains(i))
                {
                    min  = dist[i];
                    current_node = i;
                }
            }
            
           // System.out.println("Current Node:"+current_node+" "+q.size());
            if( dist[current_node] == 90000)
                break;
            q.remove(current_node);
            int k=0;
            if(current_node%boardSize != 0)
                neighbour[k++] = current_node-1;
            if( (current_node+1)%boardSize != 0)
                neighbour[k++] = current_node+1;
            if( (current_node+boardSize) < length)
                neighbour[k++] = current_node+boardSize;
            if( (current_node-boardSize) >= 0)
                neighbour[k++] = current_node-boardSize;
            
            //for each neighbor v of u
            for(int i=0;i<k;i++)
            {
                int alt = dist[current_node] + 1;
                int v = neighbour[i];
                
                x = (int)(v/boardSize);
            	y = (int)(v%boardSize);
            	int type = game.dkCellType(x, y);
            	//System.out.println("Current Vertice: "+ current_node + " Neighbour: "+v+"["+x+","+y+"] Type: "+type);
                if (alt < dist[v] && type == 2)
                {
                    dist[v] = alt ;
                    previous[v] = current_node ;
                  //  decrease-key v in Q;
                }
            }
        }
        for(int i=0;i<length;i++)
        {
            //System.out.println( "[ "+i +"] "+dist[i] +": "+ previous[i]);
        	path[i] = previous[i];
        }
    }
    
    /**
     * TODO: this method generates path from (destination_x, destination_y) 
     * to (initial_x, initial_y) given in generateMap method. If you change it
     * change gatPath method as well.
     * 
     * @param destination_x
     * @param destination_y
     */
    public void generatePath(int destination_x, int destination_y)
    {
    	int destination = destination_x*boardSize + destination_y;
    	int possible_position1,possible_position2,possible_position3,possible_position4,min=0,position;
    	
    	possible_position1 = destination-1;
    	possible_position2 = destination+1;
    	possible_position3 = destination+boardSize;
    	possible_position4 = destination-boardSize;
    	
    	if( possible_position1 >= 0 &&
    	        dist[possible_position1] < dist[possible_position2] )
    	{
    		min = dist[possible_position3];
    		position = possible_position3;
    		if(dist[possible_position1] < dist[possible_position3])
    		{
    			min = dist[possible_position1];
    			position = possible_position1;
    		}
    	}
    	else
    	{
    		min = dist[possible_position3];
    		position = possible_position3;
    		if( dist[possible_position2] < dist[possible_position3] )
    		{
    			min = dist[possible_position2];
    			position = possible_position2;
			}
    	}
    	if( possible_position4 >= 0 &&
    	        min > dist[possible_position4] )
    	{
    		min = dist[possible_position4];
    		position = possible_position4;
    	}
    	//System.out.println(position +" prev: "+ previous[position]);
    	path(position);
    }
    
    int path (int position)
    {
    	if( position < 0 )
    		return 0;
    	path_stack.add(position);
    	position = path( previous[position] );
    	return 0;
    }
    
    /**
     * Returns generated shortest path.
     * @return
     */
    public ArrayList<Point> getPath() {
        //x = rowId; y = colId;
        ArrayList<Point> path = new ArrayList<Point>();
        //now we find way from garbage to harvester, but we need otherwise
        for(int index = path_stack.size()-1; index >= 0; index--) {
            int position = path_stack.get(index);
            path.add(new Point(position / boardSize, position % boardSize));
        }
        
        return path;
    }
}
