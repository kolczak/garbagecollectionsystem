package sma.ontology;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import sma.gui.UtilsGUI;
import sma.ontology.Cell.CellFlags;

import java.util.*;
/**
 * <p><B>Title:</b> IA2-SMA</p>
 * <p><b>Description:</b> Practical exercise 2011-12. Recycle swarm.</p>
 * Information about the current game. This object is initialized from a file.
 * <p><b>Copyright:</b> Copyright (c) 2011</p>
 * <p><b>Company:</b> Universitat Rovira i Virgili (<a
 * href="http://www.urv.cat">URV</a>)</p>
 * @author David Isern & Joan Albert L�pez
 * @see sma.CoordinatorAgent
 * @see sma.CentralAgent
 */
public class InfoGame implements java.io.Serializable {

	private AuxInfo info;  //Object sent to the CoordinatorAgent during the initialization
	private List<Cell> buildingsGarbage; //List of the undiscovered buildings contain garbage.//It cannot be sent to the CoordinatorAgent.
	private int turn=0;
	private int gameDuration;
	private long timeout;
	static private float probGarbage; 
	static private boolean DEBUG = false;
	static List<Building> listBuilds;
	private static InfoGame instance = new InfoGame();
	public int building_discoverd=0;
	public int count_garbage=0;

	public static InfoGame getInstance() {
        return instance;
	}
    
	private InfoGame() {
		info=new AuxInfo();
		buildingsGarbage=new java.util.ArrayList<Cell>();
		listBuilds=new java.util.ArrayList<Building>();
	}

	public AuxInfo getInfo() {
		return info;
	}

	public void setInfo(AuxInfo info) {
		this.info = info;
	}

	private void showMessage(String s) {
		if(this.DEBUG)
			System.out.println(s);
	}
  
	public int[] getRandomXY()
	{
		Random random_place = new Random();
		int total = this.info.randXY.size();
		int index = random_place.nextInt(total);
		//System.out.println(this.info.randXY);
		int data[] =  new int[2];
		data[0] = this.info.randXY.get(index).get(0);
		data[1] = this.info.randXY.get(index).get(1);
		return data;
	}
  
	public int getTurn() { return this.turn; }
	public void incrTurn() { this.turn++; }
	public int getGameDuration() {return this.gameDuration;}
	public void setGameDuration(int d) {this.gameDuration = d;}
	public long getTimeout() {return this.timeout;}
	public void setTimeout(long n) {this.timeout = n;}	
	public float getProbGarbage() {return probGarbage;}
	public void setProbGarbage(float probGarbage) {this.probGarbage = probGarbage;}
	public boolean isEndGame() { return (this.turn>=this.getGameDuration()); }
  
	public void getMap()
	{
		for(int i=0;i<20;i++)
			for(int j=0;j<20;j++)
			{
			  if(this.info.map[i][j].isThereAnAgent())
				  System.out.println(this.info.map[i][j]+" "+this.info.map[i][j].getCellType());
			}
	}
  
	public void setNewCell(int row, int col,int num_visited)
	{
		this.info.map[row][col]= new Cell(Cell.STREET);
		this.info.map[row][col].setRow(row);
		this.info.map[row][col].setColumn(col);
		this.info.map[row][col].setVisited(num_visited);
	}

	/**
	 * We write the string specified into a file.
	 * @param content String to write
	 * @param file Pathname of the file
	 * @return Nothing
	 */
//  private void writeFile(String content, File file) throws IOException {
//    StringBuffer sb = new StringBuffer(content);
//    PrintStream outFile = new PrintStream(new FileOutputStream(file));
//    for (int i = 0; i < content.length(); i++) {
//      outFile.print(sb.charAt(i));
//    }
//    //    System.out.println(content.length()+" characters write");
//  }
	
	/**
	 *  method to write the results that was shown in the statistics tab.
	 */
	public void writeGameResult(String fileOutput, Cell[][] t) throws IOException, Exception {
		File file= new File(fileOutput);
		String content = "" + this.getGameDuration()+"\n"+this.getTimeout()+"\n";
		for(int r=0; r<t.length; r++) {
			for(int c=0; c<t[0].length; c++) {
				Cell ca = t[r][c];
				content = content + Cell.getCellType(ca.getCellType());
				if(ca.getCellType()==Cell.BUILDING)
					content = content + ca.getGarbageUnits();
				content+="\t";
			}
			content+="\n";
		}
		UtilsGUI.writeFile(content,file);
		showMessage("File written");
	}

	/**
	 * function to read the game.txt file and assign all the data in variables
	 * @param file
	 */
	public void readGameFile (String file) throws IOException,Exception {
		FileReader fis = new FileReader(file);
		BufferedReader dis = new BufferedReader(fis);
		int NROWS = 0, NCOLS = 0;
    
		String dades = dis.readLine(); StringTokenizer st = new StringTokenizer(dades, " ");
		this.setProbGarbage(Float.parseFloat(st.nextToken()));
		dades = dis.readLine(); st = new StringTokenizer(dades, " ");
		this.setGameDuration(Integer.parseInt(st.nextToken()));
		dades = dis.readLine(); st = new StringTokenizer(dades, " ");
		this.setTimeout(Long.parseLong(st.nextToken()));
		dades = dis.readLine(); st = new StringTokenizer(dades, " ");
		NROWS = Integer.parseInt(st.nextToken());
		dades = dis.readLine(); st = new StringTokenizer(dades, " ");
		NCOLS = Integer.parseInt(st.nextToken());
		this.info.map = new Cell[NROWS][NCOLS];
		dades = dis.readLine(); st = new StringTokenizer(dades, " ");
		this.info.setNumScouts(Integer.parseInt(st.nextToken()));
		dades = dis.readLine(); st = new StringTokenizer(dades, " ");
		this.info.setNumHarvesters(Integer.parseInt(st.nextToken()));
		dades = dis.readLine(); st = new StringTokenizer(dades, " ");
		dades = st.nextToken(); st = new StringTokenizer(dades, ",");
		this.info.setTypeHarvesters(new String[this.info.getNumHarvesters()]);
		this.info.setCapacityHarvesters(new int[this.info.getNumHarvesters()]);
		for (int i=0; i<this.info.getNumHarvesters(); i++) {
			String str = st.nextToken();
			StringTokenizer st2 = new StringTokenizer(str, "-");
			this.info.getTypeHarvesters()[i] = st2.nextToken();
			this.info.getCapacityHarvesters()[i] = Integer.parseInt(st2.nextToken());
		}
		int col=0, row=0;
		
		while ((dades = dis.readLine()) != null) {
			col=0;
			st = new StringTokenizer(dades, " ");
			while (st.hasMoreTokens()){
				String str = st.nextToken();
				if(str.equals("s")){
					this.info.map[row][col]= new Cell(Cell.STREET);
					
					List<Integer> list = new ArrayList<Integer>();
					list.add( row );
					list.add( col );
					this.info.setXY(list);
				}
				else{
					if(str.charAt(0)=='b') {										
					this.info.map[row][col]= new Cell(Cell.BUILDING);
					Building b = new Building();
					b.setCol(col);
					b.setRow(row);
					listBuilds.add(b);	
					if (str.length()>1){
						String type = str.substring(str.length()-2, str.length()-1);
						int units = Integer.parseInt(str.substring(2, str.length()-2));
						//We create a cell with the information about garbage that is stored
						//in the private list buildingsGarbage. The public map does not contain
						//the information about garbage, until it is discovered.
						Cell garbage = new Cell(Cell.BUILDING);
						garbage.setRow(row); garbage.setColumn(col);
						garbage.setGarbageUnits(units);
						garbage.setGarbageType(type.charAt(0));
						buildingsGarbage.add(garbage);
						//Comment
						this.info.map[row][col].setGarbageUnits(units);
						this.info.map[row][col].setGarbageType(type.charAt(0));
						this.info.map[row][col].setFlag(CellFlags.REGULAR_GARBAGE);
						this.count_garbage++;
					}					
					}else{
						this.info.map[row][col]= new Cell(Cell.RECYCLING_CENTER);
						if (str.length()>1){
							str = str.substring(2, str.length()-1);
							StringTokenizer st2 = new StringTokenizer(str, ",");
							int[] points = new int[4];
							points[0] = Integer.parseInt(st2.nextToken());
							points[1] = Integer.parseInt(st2.nextToken());
							points[2] = Integer.parseInt(st2.nextToken());
							points[3] = Integer.parseInt(st2.nextToken());
							this.info.map[row][col].setGarbagePoints(points);
							this.info.addRecyclingCenter(this.info.map[row][col]);
						}					
					}
				}
				this.info.map[row][col].setRow(row);
				this.info.map[row][col].setColumn(col);
				showMessage(((Cell)info.map[row][col]).toString() );
				col++;
			}
			row++;		
		}
		setBuildingsGarbage(buildingsGarbage);	
	}
 
	public List<Cell> getBuildingsGarbage() {
		return info.getBuildingsGarbage();
	}
	
	public List<Cell> getRecyclingCenters() {
		return info.getRecyclingCenters();
	}
	
	public void setBuildingsGarbage(List<Cell> buildingsGarbage) {
		this.buildingsGarbage = buildingsGarbage;
		info.setBuildingsGarbage(buildingsGarbage);
	}
		
	public int getCapacityHarvester(int i) {
	    return info.getCapacityHarvesters()[i];
	}
	
	public String getTypeHarvester(int i) {
	    return info.getTypeHarvesters()[i];
	}
	
	/**
	 * function that get the probability of generate garbage and get 
	 * the factor randomly if the factor is lower than the probability
	 * then call the function of generate garbage with the position of a 
	 * certain building also generated randomly.
	*/
	public void randomGenerate() throws IOException, Exception{								
		float prob = this.probGarbage*10;
		Random ran = new Random();
		int factor = Math.abs(ran.nextInt()%10);
		System.out.println("FACTOR: " + factor + ", PROBABILITY: " + prob);
		if (factor < prob){
			int bj = Math.abs(ran.nextInt()%listBuilds.size()-1);
			generateGarbage(listBuilds.get(bj).getRow(), listBuilds.get(bj).getCol());
		}		
	}
	
	/**
	 * function to generate the garbage and put in the map.
	 * @param row
	 * @param col
	 * @throws Exception
	 */
	public void generateGarbage(int row, int col) throws Exception{
	    Random ran = new Random();
	    int randType = Math.abs(ran.nextInt()%4);          
		int units = Math.abs(ran.nextInt()%10);
		Cell newgarbage = new Cell(Cell.BUILDING);
		newgarbage.setRow(row); 
		newgarbage.setColumn(col);
		newgarbage.setGarbageUnits(units);
		char type = getType(randType);
		newgarbage.setGarbageType(type);				
		buildingsGarbage.add(newgarbage);
		this.count_garbage++;
		this.info.map[row][col]= new Cell(Cell.BUILDING);
		this.info.map[row][col].setGarbageUnits(units);		
		this.info.map[row][col].setGarbageType(type);
		this.info.map[row][col].setFlag(CellFlags.REGULAR_GARBAGE);
	}
	
	/**
	 * function to get the garbage's type
	 * @param randType
	 * @return garbageType
	 */
	public char getType(int randType)
	{		
		char garbageType='-';
	    if (randType==0) //GLASS
	        garbageType = 'G';
	    if (randType==1) //PLASTIC
	        garbageType = 'P';
	    if (randType==2) //METAL
	    	garbageType = 'M';
	    if (randType==3) //PAPER
	    	garbageType = 'A';
	    return garbageType;
	}
	
	/**
	 * method to remove garbage from the cell
	 */
	public void cleanUpCell(int row, int col) {
	    this.info.map[row][col].clean();
	}
	
	/**
	 * method to mark garbage as discovered
	 */
	public void garbageDiscovered(int row, int col) {
		if(this.info.map[row][col].getFlag() != CellFlags.DISCOVERED_GARBAGE)
	    	this.building_discoverd++;
	    this.info.map[row][col].setFlag(CellFlags.DISCOVERED_GARBAGE);  
	}
	
	/**
	 * method to get how much garbage as discovered
	 */
	public int totalGarbageDiscovered() {
	    return this.building_discoverd;
	}
	
	
	/**
	 * method to get percentage garbage as discovered
	 */
	public double parcentageGarbageDiscovered() {
		double percentage = ((double)this.building_discoverd/(double)this.count_garbage)*100;
	    return percentage;
	}
	
	
	/**
     * method to mark garbage as new
     */
	public void newGarbage(int row, int col) {
        this.info.map[row][col].setFlag(CellFlags.REGULAR_GARBAGE);
    }
} //end of class InfoPartida
