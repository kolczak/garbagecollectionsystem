package sma.ontology;

public class Building {

	private int row;
	private int col;
	private static Building instance = new Building();

	public static Building getInstance() {
	    return instance;
	}
	
	public void setRow(int row) {
		this.row = row;
	}

	public int getRow() {
		return row;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public int getCol() {
		return col;
	}
	
	
}
