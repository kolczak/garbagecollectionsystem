package sma;

import sma.ScoutAgent.MyMovement;
import sma.ScoutAgent.OnceAgain;
import jade.core.Agent;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.Property;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class HarvesterAgent extends Agent{

	private String harvesterName;
	/*
	 * 
	 */
	void RegisterDF(String name) {
		if(name!=null){this.harvesterName=name;
		}else{
			this.harvesterName="HarvesterName";
		}
		DFAgentDescription dfd = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setName(name);
		sd.setType("HarvesterAgent");
		Property p = new Property();
		p.setName("up");
		p.setValue(1);
		p.setName("down");
		p.setValue(2);
		p.setName("right");
		p.setValue(3);
		p.setName("left");
		p.setValue(4);
		sd.addProperties(p);
		dfd.addServices(sd);
		dfd.setName(getAID());

		try {
		      DFService.register(this, dfd);
			  System.out.println("["+getLocalName()+"]:" + name + " has been registered in DF");
		    } catch (jade.domain.FIPAException e) { 
		      System.out.println("["+getLocalName()+"]:" + name + " hasn't been registered in DF");
		      doDelete();
		    } 
		}

	/*
	 * 
	 */
	void DeRegisterDF() {
		    try {
		      DFService.deregister(this);
		      System.out.println("["+getLocalName()+"]:"+" has been removed from DF");
		    } catch (Exception e) {
		      System.out.println("["+getLocalName()+"]:"+" hasn't been removed from DF");
		    }
		  }

	/*
	 * (non-Javadoc)
	 * @see jade.core.Agent#takeDown()
	 */
	public void takeDown(){
		    DeRegisterDF();
		}

	/*
	 * (non-Javadoc)
	 * @see jade.core.Agent#setup()
	 */
	public void setup() {
			RegisterDF("HarvesterAgent");
		    addBehaviour(new MyMovement());
		    addBehaviour(new OnceAgain(this));
	}

		  /*
		   * Class for Movements
		   */
		class MyMovement extends OneShotBehaviour {
			  
			public MyMovement(){
				super();
			}

			public void action() {
				System.out.println("Harvester World!");
				//CommunicationModule com = new CommunicationModule(this);
				try { Thread.sleep(2000); } catch (InterruptedException e) { e.printStackTrace(); }
				// iPerformative, aidReceiver, sLanguage, sOntology, sContent); //BaseCommunicationModule.send(this,null,null,"","","");												
			}			    
		  }
		  
		  /*
		   * Class for Repit Movements
		   */		  
		  class OnceAgain extends OneShotBehaviour {
			  
				public OnceAgain(Agent myagent){
					super ();
					this.myAgent=myagent;
				}

				public void action() {
					SequentialBehaviour sb = new SequentialBehaviour();
					sb.addSubBehaviour(new MyMovement());
					//sb.addSubBehaviour(new OnceAgain(myAgent));
					this.myAgent.addBehaviour(sb);
				}
				    
		  }
}
