package sma;

import java.util.Timer;
import java.util.TimerTask;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;

/**
 * Class containing common parts for all the agents in the system.
 * @author kolczak
 *
 */
public abstract class BaseAgent extends Agent {
    
    /**
     * 
     */
    private static final long serialVersionUID = -9127066809722447480L;

    /**
     * A message is shown in the log area of the GUI
     * @param str String to show
     */
    public void showMessage(String str) {
      System.out.println(getLocalName() + ": " + str);
    }

    public abstract void startCommunicationModule();
    
}
